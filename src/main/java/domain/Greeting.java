package domain;

import lombok.Value;

/**
 * Author: Łukasz Kucik
 * Company: BetterCode
 * Date: 16.01.2018
 */
public class Greeting {
  private String content;

  public Greeting() {
  }

  public Greeting(String content) {
    this.content = content;
  }

  public String getContent() {
    return content;
  }
}
