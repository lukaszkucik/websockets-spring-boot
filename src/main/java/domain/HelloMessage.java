package domain;

import lombok.Data;

/**
 * Author: Łukasz Kucik
 * Company: BetterCode
 * Date: 16.01.2018
 */
public class HelloMessage {
  private String name;

  public HelloMessage() {
  }

  public HelloMessage(String name) {
    this.name = name;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }
}
